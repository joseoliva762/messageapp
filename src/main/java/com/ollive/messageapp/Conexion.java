package com.ollive.messageapp;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author Queen
 */
public class Conexion {
    public Connection getConnection() throws SQLException{
        Connection connection = null;
        try{
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/messageApp", "root", "");
            if ( connection != null ){
                System.out.println("Conexión Establesida [OK]");
            }
        } catch (SQLException err){
            System.out.println(err);
        }
        return connection;
    }
}

