/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ollive.messageapp;

/**
 *
 * @author Queen
 */
public class Mensajes {
    int idMensaje;
    String mensaje;
    String autor_mensaje;
    String fechaMensaje;
    
    // constructors
    public Mensajes(){}

    public Mensajes(String mensaje, String autor_mensaje, String fechaMensaje) {
        this.mensaje = mensaje;
        this.autor_mensaje = autor_mensaje;
        this.fechaMensaje = fechaMensaje;
    }
    // GETTERs y SETTERs
    public int getIdMensaje() { return idMensaje; }
    public void setIdMensaje(int idMensaje) {
        this.idMensaje = idMensaje;
    }

    public String getMensaje() { return mensaje; }
    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getAutor_mensaje() { return autor_mensaje; }
    public void setAutor_mensaje(String autor_mensaje) {
        this.autor_mensaje = autor_mensaje;
    }

    public String getFechaMensaje() { return fechaMensaje; }
    public void setFechaMensaje(String fechaMensaje) {
        this.fechaMensaje = fechaMensaje;
    }
    
    
}
