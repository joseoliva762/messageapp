/*
 * Message Data Access Object
 */
package com.ollive.messageapp;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Queen
 */
public class MensajesDAO {
    public static void crearMensajeDB( Mensajes mensaje){
        Conexion dbConect = new Conexion();
        try{
            Connection conexion = dbConect.getConnection();
            PreparedStatement service = null;
            try {
                String query = "INSERT INTO `mensajes` ( mensaje, autor_mensaje )"
                            + " VALUES (?, ?)";
                service = conexion.prepareStatement(query);
                service.setString(1, mensaje.getMensaje());
                service.setString(2, mensaje.getAutor_mensaje());
                service.executeUpdate();
                System.out.println("Mensaje Creado. [OK]");
            }catch (SQLException err) {
                System.out.println(err);
            }
        } catch (SQLException err){
            System.out.println(err);
        }
    }
    public static void leerMensajesDB(){
        Conexion dbConect = new Conexion();
        try{
            Connection conexion = dbConect.getConnection();
            PreparedStatement service = null;
            ResultSet res = null;
            try {
                String query = "SELECT * FROM mensajes";
                service = conexion.prepareStatement(query);
                res = service.executeQuery();
                System.out.println("Mensajes Leidos [OK]\n");
                while(res.next()){
                    System.out.println("Id: "
                                      + res.getInt("id_mensaje")
                                      + ", Mensaje: "
                                      + res.getString("mensaje")
                                      + ", Autor: "
                                      + res.getString("autor_mensaje")
                                      + ", Fecha: "
                                      + res.getString("fecha_mensaje")
                                      + ".\n"
                    );
                            
                }
                
            }catch (SQLException err) {
                System.out.println("No se pudieron leer los mensajes [BAD]");
                System.out.println(err);
            }
        } catch (SQLException err){
            System.out.println(err);
        }
    }
    public static void borrarMensajeDB(){
        
    }
    public static void actualizarMensajeDB(){
        
    }
}
