package com.ollive.messageapp;

import java.util.Scanner;

/**
 *
 * @author Queen
 */
public class index {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int option = 0;
        
        do{
            System.out.print("------------------------\n"
                            + "Aplicacion de mensajes\n"
                            + "1. Crear mensaje.\n"
                            + "2. Listar mensaje.\n"
                            + "3. Editar mensaje.\n"
                            + "4. Borrar mensaje.\n"
                            + "5. Salir.\n>> Opcion: \n"
            ); 
            // Leemos la opcion del usuario
            option = sc.nextInt();
            switch( option ){
                case 1:
                    MensajesService.crearMensaje();
                 break;
                case 2:
                    MensajesService.listaMensajes();
                 break;
                case 3:
                    MensajesService.editarMensaje();
                 break;
                case 4:
                    MensajesService.borrarMensaje();
                 break;
                default:
                 break;
            }
        }while(option != 5);
    }
}
